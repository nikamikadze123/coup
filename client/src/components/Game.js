import React, { useEffect, useRef, useState } from 'react'
import PACK_OF_CARDS from '../utils/packOfCards'
import shuffleArray from '../utils/shuffleArray'
import io from 'socket.io-client'
import queryString from 'query-string'
import Spinner from './Spinner'

let socket
const ENDPOINT = 'http://localhost:5000'
let testTimeOut = null

const Game = (props) => {
  const data = queryString.parse(props.location.search)
  //initialize socket state
  let player1challenged = null
  let player2challenged = null
  let player3challenged = null
  let player4challenged = null
  const [room, setRoom] = useState(data.roomCode)
  const [roomFull, setRoomFull] = useState(false)
  const [users, setUsers] = useState([])
  const [currentUser, setCurrentUser] = useState('')

  useEffect(() => {
    const connectionOptions = {
      forceNew: true,
      reconnectionAttempts: 'Infinity',
      timeout: 10000,
      transports: ['websocket'],
    }
    socket = io.connect(ENDPOINT, connectionOptions)

    socket.emit('join', { room: room }, (error) => {
      if (error) setRoomFull(true)
    })

    //cleanup on component unmount
    return function cleanup() {
      socket.emit('disconnect')
      //shut down connnection instance
      socket.off()
    }
  }, [])

  //initialize game state
  const [gameOver, setGameOver] = useState(true)
  const [magicianSkill, setMagicianSkill] = useState(false)
  const [assassinSkill, setAssassinSkill] = useState(false)
  const [thiefSkill, setThiefSkill] = useState(false)
  const [dictatorSkill, setDictatorSkill] = useState(false)
  const [winner, setWinner] = useState('')
  const [turn, setTurn] = useState('')
  const [pickingCoin, setPickingCoin] = useState(false)
  const [p1HaveCard, setP1HaveCard] = useState(false)
  const [p2HaveCard, setP2HaveCard] = useState(false)
  const [p3HaveCard, setP3HaveCard] = useState(false)
  const [p4HaveCard, setP4HaveCard] = useState(false)
  const [player1Deck, setPlayer1Deck] = useState([])
  const [player2Deck, setPlayer2Deck] = useState([])
  const [player3Deck, setPlayer3Deck] = useState([])
  const [player4Deck, setPlayer4Deck] = useState([])
  const [player1Coins, setPlayer1Coins] = useState(0)
  const [player2Coins, setPlayer2Coins] = useState(0)
  const [player3Coins, setPlayer3Coins] = useState(0)
  const [player4Coins, setPlayer4Coins] = useState(0)
  const [playedCardsPile, setPlayedCardsPile] = useState([])
  const [drawCardPile, setDrawCardPile] = useState([])

  //challenge logic
  const [playedCard, setPlayedCard] = useState('')
  const [KillUrCardsStyle, setKillUrCardsStyle] = useState('Card')
  const [player1KillCard, setPlayer1KillCard] = useState(false)
  const [player2KillCard, setPlayer2KillCard] = useState(false)
  const [player3KillCard, setPlayer3KillCard] = useState(false)
  const [player4KillCard, setPlayer4KillCard] = useState(false)
  const [canChallenge1Player, setCanChallenge1Player] = useState(false)
  const [canChallenge2Player, setCanChallenge2Player] = useState(false)
  const [canChallenge3Player, setCanChallenge3Player] = useState(false)
  const [canChallenge4Player, setCanChallenge4Player] = useState(false)
  const [player1BeingChallenged, setPlayer1BeingChallenged] = useState(false)
  const [player2BeingChallenged, setPlayer2BeingChallenged] = useState(false)
  const [player3BeingChallenged, setPlayer3BeingChallenged] = useState(false)
  const [player4BeingChallenged, setPlayer4BeingChallenged] = useState(false)

  //magician skill states
  const [twoChangableCard, setTwoChangableCard] = useState([])
  const [inDrawPile, setInDrawPile] = useState(true)
  const [changingCards, setChangingCards] = useState(false)
  const [changingUrCard, setChangingUrCard] = useState(false)
  const [selectedChangerCard, setSelectedChangerCard] = useState('')
  const [changedCardStyle, setChangedCardStyle] = useState('Card')

  //thief skill states
  const [changedCoinStyle, setChangedCoinStyle] = useState('coin')
  const [canSteal, setCanSteal] = useState(false)

  //assassin skill states
  const [killingPlayer, setKillingPlayer] = useState(false)
  const [killStyleChanger, setKillStyleChanger] = useState('')
  const [player1beingKilled, setPlayer1beingKilled] = useState(false)
  const [player2beingKilled, setPlayer2beingKilled] = useState(false)
  const [player3beingKilled, setPlayer3beingKilled] = useState(false)
  const [player4beingKilled, setPlayer4beingKilled] = useState(false)
  useEffect(() => {
    //shuffle PACK_OF_CARDS array
    const shuffledCards = shuffleArray(PACK_OF_CARDS)

    const player1Deck = shuffledCards.splice(0, 2)

    const player2Deck = shuffledCards.splice(0, 2)

    const player3Deck = shuffledCards.splice(0, 2)

    const player4Deck = shuffledCards.splice(0, 2)

    const drawCardPile = shuffledCards.splice(0, 7)
    //send initial state to server
    socket.emit('initGameState', {
      gameOver: false,
      turn: 'Player 1',
      player1Deck: [...player1Deck],
      player2Deck: [...player2Deck],
      player3Deck: [...player3Deck],
      player4Deck: [...player4Deck],
      player1Coins: 2,
      player2Coins: 2,
      player3Coins: 2,
      player4Coins: 2,
      playedCardsPile: [...playedCardsPile],
      drawCardPile: [...drawCardPile],
    })
  }, [])

  useEffect(() => {
    socket.on(
      'initGameState',
      ({
        gameOver,
        turn,
        player1Deck,
        player2Deck,
        player3Deck,
        player4Deck,
        player1Coins,
        player2Coins,
        player3Coins,
        player4Coins,
        playedCardsPile,
        drawCardPile,
      }) => {
        setGameOver(gameOver)
        setTurn(turn)
        setPlayer1Deck(player1Deck)
        setPlayer2Deck(player2Deck)
        setPlayer3Deck(player3Deck)
        setPlayer4Deck(player4Deck)
        setPlayer1Coins(player1Coins)
        setPlayer2Coins(player2Coins)
        setPlayer3Coins(player3Coins)
        setPlayer4Coins(player4Coins)
        setPlayedCardsPile(playedCardsPile)
        setDrawCardPile(drawCardPile)
      }
    )

    socket.on(
      'updateGameState',
      ({
        gameOver,
        winner,
        turn,
        player1KillCard,
        player2KillCard,
        player3KillCard,
        player4KillCard,
        player1BeingChallenged,
        player2BeingChallenged,
        player3BeingChallenged,
        player4BeingChallenged,
        canChallenge1Player,
        canChallenge2Player,
        canChallenge3Player,
        canChallenge4Player,
        player1HaveCard,
        player2HaveCard,
        player3HaveCard,
        player4HaveCard,
        player1beingKilled,
        player2beingKilled,
        player3beingKilled,
        player4beingKilled,
        player1Deck,
        player2Deck,
        player3Deck,
        player4Deck,
        player1Coins,
        player2Coins,
        player3Coins,
        player4Coins,
        playedCard,
        playedCardsPile,
        drawCardPile,
      }) => {
        gameOver && setGameOver(gameOver)
        winner && setWinner(winner)
        turn && setTurn(turn)
        player1KillCard && setPlayer1KillCard(player1KillCard)
        player2KillCard && setPlayer2KillCard(player2KillCard)
        player3KillCard && setPlayer3KillCard(player3KillCard)
        player4KillCard && setPlayer4KillCard(player4KillCard)
        player1BeingChallenged &&
          setPlayer1BeingChallenged(player1BeingChallenged)
        player2BeingChallenged &&
          setPlayer2BeingChallenged(player2BeingChallenged)
        player3BeingChallenged &&
          setPlayer3BeingChallenged(player3BeingChallenged)
        player4BeingChallenged &&
          setPlayer4BeingChallenged(player4BeingChallenged)
        canChallenge1Player && setCanChallenge1Player(canChallenge1Player)
        canChallenge2Player && setCanChallenge2Player(canChallenge2Player)
        canChallenge3Player && setCanChallenge3Player(canChallenge3Player)
        canChallenge4Player && setCanChallenge4Player(canChallenge4Player)
        player1HaveCard && setP1HaveCard(player1HaveCard)
        player2HaveCard && setP2HaveCard(player2HaveCard)
        player3HaveCard && setP3HaveCard(player3HaveCard)
        player4HaveCard && setP4HaveCard(player4HaveCard)
        player1beingKilled && setPlayer1beingKilled(player1beingKilled)
        player2beingKilled && setPlayer2beingKilled(player2beingKilled)
        player3beingKilled && setPlayer3beingKilled(player3beingKilled)
        player4beingKilled && setPlayer4beingKilled(player4beingKilled)
        player1Deck && setPlayer1Deck(player1Deck)
        player2Deck && setPlayer2Deck(player2Deck)
        player3Deck && setPlayer3Deck(player3Deck)
        player4Deck && setPlayer4Deck(player4Deck)
        player1Coins && setPlayer1Coins(player1Coins)
        player2Coins && setPlayer2Coins(player2Coins)
        player3Coins && setPlayer3Coins(player3Coins)
        player4Coins && setPlayer4Coins(player4Coins)
        playedCard && setPlayedCard(playedCard)
        playedCardsPile && setPlayedCardsPile(playedCardsPile)
        drawCardPile && setDrawCardPile(drawCardPile)
      }
    )

    socket.on('clearTimeOut', (name) => {
      clearTimeout(testTimeOut)
    })

    socket.on('roomData', ({ users }) => {
      setUsers(users)
    })

    socket.on('currentUserData', ({ name }) => {
      setCurrentUser(name)
    })
  }, [])

  useEffect(() => {
    if (player1Deck.includes('MAF')) {
      socket.emit('updateGameState', {
        player1HaveCard: true,
      })
    }
    if (player2Deck.includes('MAF')) {
      socket.emit('updateGameState', {
        player2HaveCard: true,
      })
    }
    if (player3Deck.includes('MAF')) {
      socket.emit('updateGameState', {
        player3HaveCard: true,
      })
    }
    if (player4Deck.includes('MAF')) {
      socket.emit('updateGameState', {
        player4HaveCard: true,
      })
    }
  }, [
    player1beingKilled,
    player2beingKilled,
    player3beingKilled,
    player4beingKilled,
  ])

  useEffect(() => {
    if (player1BeingChallenged) {
      if (currentUser === 'Player 1') {
        if (player1KillCard) {
          setKillUrCardsStyle('kill-style')
        }
      }
    }
  }, [player1BeingChallenged])
  useEffect(() => {
    if (player2BeingChallenged) {
      if (currentUser === 'Player 2') {
        if (player2KillCard) {
          setKillUrCardsStyle('kill-style')
        }
      }
    }
  }, [player2BeingChallenged])
  useEffect(() => {
    if (player3BeingChallenged) {
      if (currentUser === 'Player 3') {
        if (player3KillCard) {
          setKillUrCardsStyle('kill-style')
        }
      }
    }
  }, [player3BeingChallenged])
  useEffect(() => {
    if (player4BeingChallenged) {
      if (currentUser === 'Player 4') {
        if (player4KillCard) {
          setKillUrCardsStyle('kill-style')
        }
      }
    }
  }, [player4BeingChallenged])
  useEffect(() => {
    if (canChallenge1Player === true && player1BeingChallenged === false) {
      player1challenged = setTimeout(() => {
        setCanChallenge1Player((prev) => !prev)
      }, 5000)
    }
  }, [canChallenge1Player])
  useEffect(() => {
    if (canChallenge2Player === true) {
      setTimeout(() => {
        setCanChallenge2Player((prev) => !prev)
      }, 5000)
    }
  }, [canChallenge2Player])
  useEffect(() => {
    if (canChallenge3Player === true) {
      setTimeout(() => {
        setCanChallenge3Player((prev) => !prev)
      }, 5000)
    }
  }, [canChallenge3Player])
  useEffect(() => {
    if (canChallenge4Player === true) {
      setTimeout(() => {
        setCanChallenge4Player((prev) => !prev)
      }, 5000)
    }
  }, [canChallenge4Player])

  const move = useRef()
  //pop up buttons
  function CardHandler(card) {
    switch (card) {
      case 'A':
        setAssassinSkill((prevVal) => !prevVal)
        setDictatorSkill(false)
        setThiefSkill(false)
        setMagicianSkill(false)
        break
      case 'D':
        setDictatorSkill((prevVal) => !prevVal)
        setThiefSkill(false)
        setMagicianSkill(false)
        setAssassinSkill(false)
        break
      case 'T':
        setThiefSkill((prevVal) => !prevVal)
        setMagicianSkill(false)
        setAssassinSkill(false)
        setDictatorSkill(false)
        break
      case 'M':
        setMagicianSkill((prevVal) => !prevVal)
        setAssassinSkill(false)
        setDictatorSkill(false)
        setThiefSkill(false)
        break
      default:
        break
    }
  }
  //Cards Skills Logic
  function CardSkill(skill) {
    switch (turn) {
      case 'Player 1':
        if (skill === 'pick') {
          socket.emit('updateGameState', {
            canChallenge1Player: true,
            playedCard: 'D',
          })
          testTimeOut = setTimeout(() => {
            console.log('settimeout still works')
            socket.emit('updateGameState', {
              turn: 'Player 2',
              player1Coins: Number(player1Coins) + 3,
            })
          }, 5000)
        } else if (skill === 'change') {
          socket.emit('updateGameState', {
            canChallenge1Player: true,
            playedCard: 'M',
          })
          testTimeOut = setTimeout(() => {
            setInDrawPile(true)
            const drawCardPileCopy = drawCardPile
            const shuffledDrawCardPile = shuffleArray(drawCardPileCopy)
            const changableCards = shuffledDrawCardPile.splice(0, 2)
            setChangingCards(true)
            setTwoChangableCard(changableCards)
          }, 5000)
        } else if (skill === 'kill') {
          socket.emit('updateGameState', {
            playedCard: 'A',
          })
          if (
            (turn === 'Player 1' && Number(player1Coins) >= 3) ||
            (turn === 'Player 2' && Number(player2Coins) >= 3) ||
            (turn === 'Player 3' && Number(player3Coins) >= 3) ||
            (turn === 'Player 4' && Number(player4Coins) >= 3)
          ) {
            setKillingPlayer(true)
            setKillStyleChanger('kill-style')
          }
        } else if (skill === 'steal') {
          socket.emit('updateGameState', {
            playedCard: 'T',
          })
          setChangedCoinStyle('steal-coin')
          setCanSteal(true)
        }
        break
      case 'Player 2':
        if (skill === 'pick') {
          socket.emit('updateGameState', {
            canChallenge2Player: true,
            playedCard: 'D',
          })
          testTimeOut = setTimeout(() => {
            socket.emit('updateGameState', {
              turn: 'Player 3',
              player2Coins: Number(player2Coins) + 3,
            })
          }, 5000)
        } else if (skill === 'change') {
          socket.emit('updateGameState', {
            canChallenge2Player: true,
            playedCard: 'M',
          })
          setInDrawPile(true)
          const drawCardPileCopy = drawCardPile
          const shuffledDrawCardPile = shuffleArray(drawCardPileCopy)
          const changableCards = shuffledDrawCardPile.splice(0, 2)
          setChangingCards(true)
          setTwoChangableCard(changableCards)
        } else if (skill === 'kill') {
          socket.emit('updateGameState', {
            playedCard: 'A',
          })
          if (
            (turn === 'Player 1' && Number(player1Coins) >= 3) ||
            (turn === 'Player 2' && Number(player2Coins) >= 3) ||
            (turn === 'Player 3' && Number(player3Coins) >= 3) ||
            (turn === 'Player 4' && Number(player4Coins) >= 3)
          ) {
            setKillingPlayer(true)
            setKillStyleChanger('kill-style')
          }
        } else if (skill === 'steal') {
          socket.emit('updateGameState', {
            playedCard: 'T',
          })
          setChangedCoinStyle('steal-coin')
          setCanSteal(true)
        }
        break
      case 'Player 3':
        if (skill === 'pick') {
          socket.emit('updateGameState', {
            canChallenge3Player: true,
            playedCard: 'D',
          })
          testTimeOut = setTimeout(() => {
            socket.emit('updateGameState', {
              turn: 'Player 4',
              player3Coins: Number(player3Coins) + 3,
            })
          }, 5000)
        } else if (skill === 'change') {
          socket.emit('updateGameState', {
            playedCard: 'M',
          })
          setInDrawPile(true)
          const drawCardPileCopy = drawCardPile
          const shuffledDrawCardPile = shuffleArray(drawCardPileCopy)
          const changableCards = shuffledDrawCardPile.splice(0, 2)
          setChangingCards(true)
          setTwoChangableCard(changableCards)
        } else if (skill === 'kill') {
          socket.emit('updateGameState', {
            playedCard: 'A',
          })
          if (
            (turn === 'Player 1' && Number(player1Coins) >= 3) ||
            (turn === 'Player 2' && Number(player2Coins) >= 3) ||
            (turn === 'Player 3' && Number(player3Coins) >= 3) ||
            (turn === 'Player 4' && Number(player4Coins) >= 3)
          ) {
            setKillingPlayer(true)
            setKillStyleChanger('kill-style')
          }
        } else if (skill === 'steal') {
          socket.emit('updateGameState', {
            playedCard: 'T',
          })
          setChangedCoinStyle('steal-coin')
          setCanSteal(true)
        }
        break
      case 'Player 4':
        if (skill === 'pick') {
          socket.emit('updateGameState', {
            canChallenge4Player: true,
            playedCard: 'D',
          })
          testTimeOut = setTimeout(() => {
            socket.emit('updateGameState', {
              turn: 'Player 1',
              player4Coins: Number(player4Coins) + 3,
            })
          }, 5000)
        } else if (skill === 'change') {
          socket.emit('updateGameState', {
            playedCard: 'M',
          })
          setInDrawPile(true)
          const drawCardPileCopy = drawCardPile
          const shuffledDrawCardPile = shuffleArray(drawCardPileCopy)
          const changableCards = shuffledDrawCardPile.splice(0, 2)
          setChangingCards(true)
          setTwoChangableCard(changableCards)
        } else if (skill === 'kill') {
          socket.emit('updateGameState', {
            playedCard: 'A',
          })
          if (
            (turn === 'Player 1' && Number(player1Coins) >= 3) ||
            (turn === 'Player 2' && Number(player2Coins) >= 3) ||
            (turn === 'Player 3' && Number(player3Coins) >= 3) ||
            (turn === 'Player 4' && Number(player4Coins) >= 3)
          ) {
            setKillingPlayer(true)
            setKillStyleChanger('kill-style')
          }
        } else if (skill === 'steal') {
          socket.emit('updateGameState', {
            playedCard: 'T',
          })
          setChangedCoinStyle('steal-coin')
          setCanSteal(true)
        }
        break
      default:
        break
    }

    setDictatorSkill(false)
    setThiefSkill(false)
    setMagicianSkill(false)
    setAssassinSkill(false)
  }
  function ChooseCard(card) {
    setChangedCardStyle('changing-card')
    setChangingUrCard(true)
    setSelectedChangerCard(card)
  }
  function endChallenge(player) {
    switch (player) {
      case 'first':
        socket.emit('updateGameState', {
          turn: 'Player 2',
          player1Coins: Number(player1Coins) + 3,
        })
        console.log('turn changed')
        break
      case 'second':
        socket.emit('updateGameState', {
          turn: 'Player 3',
          player2Coins: Number(player2Coins) + 3,
        })
        break
      case 'third':
        socket.emit('updateGameState', {
          turn: 'Player 4',
          player3Coins: Number(player3Coins) + 3,
        })
        break
      case 'fourth':
        socket.emit('updateGameState', {
          turn: 'Player 1',
          player4Coins: Number(player4Coins) + 3,
        })
        break

      default:
        break
    }
  }
  function swap(card) {
    const changercardindex = twoChangableCard.indexOf(selectedChangerCard)
    const removedchanger = twoChangableCard.splice(changercardindex, 1)

    switch (turn) {
      case 'Player 1':
        var yourcardindex1 = player1Deck.indexOf(card)
        var removeurcard1 = player1Deck.splice(yourcardindex1, 1)
        var updatedDrawPile1 = [...drawCardPile, removeurcard1]
        var updatedDeck1 = [...player1Deck, removedchanger[0]]

        socket.emit('updateGameState', {
          turn: 'Player 2',
          player1Deck: updatedDeck1,
          drawCardPile: updatedDrawPile1,
        })
        setTwoChangableCard([...twoChangableCard, removeurcard1])
        break
      case 'Player 2':
        var yourcardindex2 = player2Deck.indexOf(card)
        var removeurcard2 = player2Deck.splice(yourcardindex2, 1)
        var updatedDrawPile2 = [...drawCardPile, removeurcard2]
        var updatedDeck2 = [...player2Deck, removedchanger[0]]

        socket.emit('updateGameState', {
          turn: 'Player 3',
          player2Deck: updatedDeck2,
          drawCardPile: updatedDrawPile2,
        })
        setTwoChangableCard([...twoChangableCard, removeurcard2])
        break
      case 'Player 3':
        var yourcardindex3 = player3Deck.indexOf(card)
        var removeurcard3 = player3Deck.splice(yourcardindex3, 1)
        var updatedDrawPile3 = [...drawCardPile, removeurcard3]
        var updatedDeck3 = [...player3Deck, removedchanger[0]]
        socket.emit('updateGameState', {
          turn: 'Player 4',
          player3Deck: updatedDeck3,
          drawCardPile: updatedDrawPile3,
        })
        setTwoChangableCard([...twoChangableCard, removeurcard3])
        break
      case 'Player 4':
        var yourcardindex4 = player4Deck.indexOf(card)
        var removeurcard4 = player4Deck.splice(yourcardindex4, 1)
        var updatedDrawPile4 = [...drawCardPile, removeurcard4]
        var updatedDeck4 = [...player4Deck, removedchanger[0]]
        socket.emit('updateGameState', {
          turn: 'Player 1',
          player4Deck: updatedDeck4,
          drawCardPile: updatedDrawPile4,
        })
        setTwoChangableCard([...twoChangableCard, removeurcard4])
        break

      default:
        break
    }
    setChangingUrCard(false)
    setChangingCards(false)
    setInDrawPile(false)
  }
  function steal(player) {
    if (
      (player === 'first' && player1Coins >= 2) ||
      (player === 'second' && player2Coins >= 2) ||
      (player === 'third' && player3Coins >= 2) ||
      (player === 'fourth' && player4Coins >= 2)
    ) {
      if (player === 'first' && player1Coins >= 2) {
        if (player1Coins === 2) {
          socket.emit('updateGameState', {
            player1Coins: '0',
          })
        }
        socket.emit('updateGameState', {
          player1Coins: player1Coins - 2,
        })
      } else if (player === 'second' && player2Coins >= 2) {
        if (player2Coins === 2) {
          socket.emit('updateGameState', {
            player2Coins: '0',
          })
        }
        socket.emit('updateGameState', {
          player2Coins: player2Coins - 2,
        })
      } else if (player === 'third' && player3Coins >= 2) {
        if (player3Coins === 2) {
          socket.emit('updateGameState', {
            player3Coins: '0',
          })
        }
        socket.emit('updateGameState', {
          player3Coins: player3Coins - 2,
        })
      } else if (player === 'fourth' && player4Coins >= 2) {
        if (player4Coins === 2) {
          socket.emit('updateGameState', {
            player4Coins: '0',
          })
        }
        socket.emit('updateGameState', {
          player4Coins: player4Coins - 2,
        })
      }
      switch (turn) {
        case 'Player 1':
          socket.emit('updateGameState', {
            turn: 'Player 2',
            player1Coins: Number(player1Coins) + 2,
          })
          break
        case 'Player 2':
          socket.emit('updateGameState', {
            turn: 'Player 3',
            player2Coins: Number(player2Coins) + 2,
          })
          break
        case 'Player 3':
          socket.emit('updateGameState', {
            turn: 'Player 4',
            player3Coins: Number(player3Coins) + 2,
          })
          break
        case 'Player 4':
          socket.emit('updateGameState', {
            turn: 'Player 1',
            player4Coins: Number(player4Coins) + 2,
          })
          break

        default:
          break
      }
      setChangedCoinStyle('coin')
    }
  }
  function kill(player) {
    switch (player) {
      case 'first':
        socket.emit('updateGameState', {
          player1beingKilled: true,
        })
        break
      case 'second':
        socket.emit('updateGameState', {
          player2beingKilled: true,
        })
        break
      case 'third':
        socket.emit('updateGameState', {
          player3beingKilled: true,
        })
        break
      case 'fourth':
        socket.emit('updateGameState', {
          player4beingKilled: true,
        })
        break

      default:
        break
    }
    setKillStyleChanger('')
  }
  function Block() {
    setKillStyleChanger('')
    setKillingPlayer(false)
    setPlayer1beingKilled(false)
    setPlayer2beingKilled(false)
    setPlayer3beingKilled(false)
    setPlayer4beingKilled(false)
    switch (turn) {
      case 'Player 1':
        socket.emit('updateGameState', {
          turn: 'Player 2',
        })
        break
      case 'Player 2':
        socket.emit('updateGameState', {
          turn: 'Player 3',
        })
        break
      case 'Player 3':
        socket.emit('updateGameState', {
          turn: 'Player 4',
        })
        break
      case 'Player 4':
        socket.emit('updateGameState', {
          turn: 'Player 1',
        })
        break

      default:
        break
    }
  }
  function pickCoin(coin) {
    switch (turn) {
      case 'Player 1':
        if (coin === 'one') {
          socket.emit('updateGameState', {
            turn: 'Player 2',
            player1Coins: Number(player1Coins) + 1,
          })
        } else {
          socket.emit('updateGameState', {
            turn: 'Player 2',
            player1Coins: Number(player1Coins) + 2,
          })
        }
        break
      case 'Player 2':
        if (coin === 'one') {
          socket.emit('updateGameState', {
            turn: 'Player 3',
            player2Coins: Number(player2Coins) + 1,
          })
        } else {
          socket.emit('updateGameState', {
            turn: 'Player 3',
            player2Coins: Number(player2Coins) + 2,
          })
        }
        break
      case 'Player 3':
        if (coin === 'one') {
          socket.emit('updateGameState', {
            turn: 'Player 4',
            player3Coins: Number(player3Coins) + 1,
          })
        } else {
          socket.emit('updateGameState', {
            turn: 'Player 4',
            player3Coins: Number(player3Coins) + 2,
          })
        }
        break
      case 'Player 4':
        if (coin === 'one') {
          socket.emit('updateGameState', {
            turn: 'Player 1',
            player4Coins: Number(player4Coins) + 1,
          })
        } else {
          socket.emit('updateGameState', {
            turn: 'Player 1',
            player4Coins: Number(player4Coins) + 2,
          })
        }
        break

      default:
        break
    }
    setPickingCoin(false)
  }
  function challenge(player) {
    socket.emit('clearSomeonesTimeOut', 'testTimeOut')
    switch (player) {
      case 'first':
        if (!player1Deck.includes(playedCard)) {
          socket.emit('updateGameState', {
            player1BeingChallenged: true,
            player1KillCard: true,
          })
        } else {
          socket.emit('updateGameState', {
            player1BeingChallenged: true,
            player1KillCard: false,
          })
        }
        break
      case 'second':
        if (!player2Deck.includes(playedCard)) {
          socket.emit('updateGameState', {
            player2BeingChallenged: true,
            player2KillCard: true,
          })
        } else {
          socket.emit('updateGameState', {
            player2BeingChallenged: true,
            player2KillCard: false,
          })
        }
        break
      case 'third':
        if (!player3Deck.includes(playedCard)) {
          socket.emit('updateGameState', {
            player3BeingChallenged: true,
            player3KillCard: true,
          })
        } else {
          socket.emit('updateGameState', {
            player3BeingChallenged: true,
            player3KillCard: false,
          })
        }
        break
      case 'fourth':
        if (!player4Deck.includes(playedCard)) {
          socket.emit('updateGameState', {
            player4BeingChallenged: true,
            player4KillCard: true,
          })
        } else {
          socket.emit('updateGameState', {
            player4BeingChallenged: true,
            player4KillCard: false,
          })
        }
        break

      default:
        break
    }
  }
  function bluff() {
    if (
      dictatorSkill === true ||
      thiefSkill === true ||
      assassinSkill === true ||
      magicianSkill === true
    ) {
      setDictatorSkill(false)
      setThiefSkill(false)
      setMagicianSkill(false)
      setAssassinSkill(false)
    }
    if (
      dictatorSkill === true &&
      thiefSkill === true &&
      assassinSkill === true &&
      magicianSkill === true
    ) {
      setDictatorSkill(true)
      setThiefSkill(true)
      setMagicianSkill(true)
      setAssassinSkill(true)
    }

    setDictatorSkill((prev) => !prev)
    setThiefSkill((prev) => !prev)
    setMagicianSkill((prev) => !prev)
    setAssassinSkill((prev) => !prev)
  }
  function killYourCard(card) {
    setKillUrCardsStyle('Card')
    setPlayer1BeingChallenged(false)
    setPlayer2BeingChallenged(false)
    setPlayer3BeingChallenged(false)
    setPlayer4BeingChallenged(false)
    setPlayer1KillCard(false)
    setPlayer2KillCard(false)
    setPlayer3KillCard(false)
    setPlayer4KillCard(false)
    switch (turn) {
      case 'Player 1':
        var yourcardindex1 = player1Deck.indexOf(card)
        var removeurcard1 = player1Deck.splice(yourcardindex1, 1)
        socket.emit('updateGameState', {
          turn: 'Player 2',
          player1Deck: player1Deck,
        })
        break
      case 'Player 2':
        var yourcardindex2 = player2Deck.indexOf(card)
        var removeurcard2 = player2Deck.splice(yourcardindex2, 1)

        socket.emit('updateGameState', {
          turn: 'Player 3',
          player2Deck: player2Deck,
        })
        break
      case 'Player 3':
        var yourcardindex3 = player3Deck.indexOf(card)
        var removeurcard3 = player3Deck.splice(yourcardindex3, 1)
        socket.emit('updateGameState', {
          turn: 'Player 4',
          player3Deck: player3Deck,
        })
        break
      case 'Player 4':
        var yourcardindex4 = player4Deck.indexOf(card)
        var removeurcard4 = player4Deck.splice(yourcardindex4, 1)
        socket.emit('updateGameState', {
          turn: 'Player 1',
          player4Deck: player4Deck,
        })
        break

      default:
        break
    }
  }
  return (
    <div className='container'>
      {!roomFull ? (
        <>
          {users.length < 4 && (
            <>
              <div className='topInfo'>
                <img src={require('../assets/logo.png').default} alt='logo' />
                <h1>Game Code: {room}</h1>
                <h3>how to play?</h3>
              </div>
              <h1 className='topInfoText'>
                Waiting for Players to join the game.
              </h1>
              <a href='/' style={{ marginLeft: '20px' }}>
                <button className='game-button'>QUIT</button>
              </a>
            </>
          )}

          {users.length === 4 && (
            <>
              <div className='coin-stash'>
                <img
                  src={require(`../assets/coinstash.png`).default}
                  onClick={() => setPickingCoin((prevVal) => !prevVal)}
                  style={{ width: '80%' }}
                  alt='stash'
                />
                {pickingCoin && (
                  <div>
                    <button onClick={() => pickCoin('one')}>Pick 1</button>
                    <button onClick={() => pickCoin('two')}>Pick 2</button>
                  </div>
                )}
              </div>
              {gameOver ? (
                <div>
                  {winner !== '' && (
                    <>
                      <h1>GAME OVER</h1>
                      <h2>{winner} wins!</h2>
                    </>
                  )}
                </div>
              ) : (
                <div>
                  {/* PLAYER 1 VIEW */}
                  {currentUser === 'Player 1' && (
                    <>
                      <div className='top'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('third')
                            }
                          }}
                        >
                          {player3Deck.map((item, i) => (
                            <img
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                              alt='card'
                            />
                          ))}
                        </div>
                        <div
                          className={changedCoinStyle}
                          onClick={() => {
                            if (canSteal) {
                              steal('third')
                            }
                          }}
                        >
                          {player3Coins}
                        </div>
                        {turn === 'Player 3' && <Spinner />}
                      </div>
                      <div className='mid'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('second')
                            }
                          }}
                        >
                          {player2Deck.map((item, i) => (
                            <img
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                              alt='card'
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('second')
                              }
                            }}
                          >
                            {player2Coins}
                          </div>
                          {turn === 'Player 2' && <Spinner />}
                        </div>
                        <div>
                          {player1BeingChallenged && (
                            <h1>You Are Being Challenged</h1>
                          )}
                          {player1KillCard && (
                            <div>
                              <h3>You Lied, Kill 1 Card!</h3>
                            </div>
                          )}
                        </div>
                        {player1beingKilled && (
                          <div>
                            <h3> You are being killed</h3>
                            <button
                              style={{
                                backgroundColor: p1HaveCard ? 'green' : 'red',
                              }}
                              onClick={() => Block()}
                            >
                              Block
                            </button>
                            <button>Kill 1 Card</button>
                            <button>Challenge</button>
                          </div>
                        )}
                        {canChallenge2Player && (
                          <button onClick={() => challenge('second')}>
                            Challenge 2nd Player
                          </button>
                        )}
                        {canChallenge3Player && (
                          <button onClick={() => challenge('third')}>
                            Challenge 3rd Player
                          </button>
                        )}
                        {canChallenge4Player && (
                          <button onClick={() => challenge('fourth')}>
                            Challenge 4th Player
                          </button>
                        )}
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('fourth')
                            }
                          }}
                        >
                          {player4Deck.map((item, i) => (
                            <img
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                              alt='card'
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('fourth')
                              }
                            }}
                          >
                            {player4Coins}
                          </div>
                          {turn === 'Player 4' && <Spinner />}
                        </div>
                      </div>
                      <div className='bot'>
                        {/* card skill buttons */}
                        <div>
                          {changingCards && (
                            <div>
                              {console.log(changedCardStyle)}
                              {twoChangableCard.map((card, i) => (
                                <img
                                  key={i}
                                  className={changedCardStyle}
                                  src={
                                    require(`../assets/cards-front/${card}.png`)
                                      .default
                                  }
                                  alt='card'
                                  onClick={() => {
                                    if (inDrawPile) {
                                      ChooseCard(card)
                                    } else {
                                      if (turn === 'Player 1') {
                                        if (changingUrCard) {
                                          swap(card)
                                        } else {
                                          CardHandler(card)
                                        }
                                      }
                                    }
                                  }}
                                />
                              ))}
                            </div>
                          )}

                          <div>
                            {magicianSkill && (
                              <button onClick={() => CardSkill('change')}>
                                Change Cards
                              </button>
                            )}
                            {assassinSkill && (
                              <button onClick={() => CardSkill('kill')}>
                                Kill with 3 Coin
                              </button>
                            )}
                            {dictatorSkill && (
                              <button onClick={() => CardSkill('pick')}>
                                Pick 3 Coin
                              </button>
                            )}
                            {thiefSkill && (
                              <button onClick={() => CardSkill('steal')}>
                                Steal 2 Coin
                              </button>
                            )}
                          </div>
                          <div ref={move}></div>
                          <img
                            className='Card'
                            src={require(`../assets/mystery-card.png`).default}
                            onClick={() => {
                              if (turn === 'Player 1') {
                                bluff()
                              }
                            }}
                            alt='bluff'
                          />
                          {player1Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className={KillUrCardsStyle}
                              src={
                                require(`../assets/cards-front/${item}.png`)
                                  .default
                              }
                              onClick={() => {
                                if (turn === 'Player 1') {
                                  if (KillUrCardsStyle === 'kill-style') {
                                    killYourCard(item)
                                  } else if (changingUrCard) {
                                    swap(item)
                                  } else {
                                    CardHandler(item)
                                  }
                                }
                              }}
                            />
                          ))}
                        </div>

                        <div className={changedCoinStyle}>{player1Coins}</div>
                        <a href='/'>
                          <button className='game-button'>QUIT</button>
                        </a>
                      </div>
                    </>
                  )}

                  {/* PLAYER 2 VIEW */}
                  {currentUser === 'Player 2' && (
                    <>
                      <div className='top'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('fourth')
                            }
                          }}
                        >
                          {player4Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                        </div>
                        <div
                          className={changedCoinStyle}
                          onClick={() => {
                            if (canSteal) {
                              steal('fourth')
                            }
                          }}
                        >
                          {player4Coins}
                        </div>
                        {turn === 'Player 4' && <Spinner />}
                      </div>
                      <div className='mid'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('third')
                            }
                          }}
                        >
                          {player3Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('third')
                              }
                            }}
                          >
                            {player3Coins}
                          </div>
                          {turn === 'Player 3' && <Spinner />}
                        </div>
                        <div>
                          {player2BeingChallenged && (
                            <h1>You Are Being Challenged</h1>
                          )}
                          {player2KillCard && (
                            <div>
                              <h3>You Lied, Kill 1 Card!</h3>
                            </div>
                          )}
                        </div>
                        {player2beingKilled && (
                          <div>
                            <h3> You are being killed</h3>
                            <button
                              style={{
                                backgroundColor: p2HaveCard ? 'green' : 'red',
                              }}
                              onClick={() => Block()}
                            >
                              Block
                            </button>
                            <button>Kill 1 Card</button>
                            <button>Challenge</button>
                          </div>
                        )}
                        {canChallenge1Player && (
                          <button onClick={() => challenge('first')}>
                            Challenge 1st Player
                          </button>
                        )}
                        {canChallenge3Player && (
                          <button onClick={() => challenge('third')}>
                            Challenge 3rd Player
                          </button>
                        )}
                        {canChallenge4Player && (
                          <button onClick={() => challenge('fourth')}>
                            Challenge 4th Player
                          </button>
                        )}
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('first')
                            }
                          }}
                        >
                          {player1Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('first')
                              }
                            }}
                          >
                            {player1Coins}
                          </div>
                          {turn === 'Player 1' && <Spinner />}
                        </div>
                      </div>
                      <div className='bot'>
                        <div>
                          {changingCards && (
                            <div>
                              {twoChangableCard.map((card, i) => (
                                <img
                                  alt='card'
                                  key={i}
                                  className={changedCardStyle}
                                  src={
                                    require(`../assets/cards-front/${card}.png`)
                                      .default
                                  }
                                  onClick={() => ChooseCard(card)}
                                />
                              ))}
                            </div>
                          )}

                          <div>
                            {magicianSkill && (
                              <button onClick={() => CardSkill('change')}>
                                Change Cards
                              </button>
                            )}
                            {assassinSkill && (
                              <button onClick={() => CardSkill('kill')}>
                                Kill with 3 Coin
                              </button>
                            )}
                            {dictatorSkill && (
                              <button onClick={() => CardSkill('pick')}>
                                Pick 3 Coin
                              </button>
                            )}
                            {thiefSkill && (
                              <button onClick={() => CardSkill('steal')}>
                                Steal 2 Coin
                              </button>
                            )}
                          </div>

                          <div ref={move}></div>
                          <img
                            alt='card'
                            className='Card'
                            src={require(`../assets/mystery-card.png`).default}
                            onClick={() => bluff()}
                          />
                          {player2Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className={KillUrCardsStyle}
                              src={
                                require(`../assets/cards-front/${item}.png`)
                                  .default
                              }
                              onClick={() => {
                                if (turn === 'Player 2') {
                                  if (KillUrCardsStyle === 'kill-style') {
                                    killYourCard(item)
                                  } else if (changingUrCard) {
                                    swap(item)
                                  } else {
                                    CardHandler(item)
                                  }
                                }
                              }}
                            />
                          ))}
                        </div>
                        <div className={changedCoinStyle}>{player2Coins}</div>
                        <br />
                        <a href='/'>
                          <button className='game-button'>QUIT</button>
                        </a>
                      </div>
                    </>
                  )}

                  {/* PLAYER 3 VIEW */}
                  {currentUser === 'Player 3' && (
                    <>
                      <div className='top'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('first')
                            }
                          }}
                        >
                          {player1Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                        </div>
                        <div
                          className={changedCoinStyle}
                          onClick={() => {
                            if (canSteal) {
                              steal('first')
                            }
                          }}
                        >
                          {player1Coins}
                        </div>
                        {turn === 'Player 1' && <Spinner />}
                      </div>
                      <div className='mid'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('fourth')
                            }
                          }}
                        >
                          {player4Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('fourth')
                              }
                            }}
                          >
                            {player4Coins}
                          </div>
                          {turn === 'Player 4' && <Spinner />}
                        </div>
                        <div>
                          {player3BeingChallenged && (
                            <h1>You Are Being Challenged</h1>
                          )}
                          {player3KillCard && (
                            <div>
                              <h3>You Lied, Kill 1 Card!</h3>
                            </div>
                          )}
                        </div>
                        {player3beingKilled && (
                          <div>
                            <h3> You are being killed</h3>
                            <button
                              style={{
                                backgroundColor: p3HaveCard ? 'green' : 'red',
                              }}
                              onClick={() => Block()}
                            >
                              Block
                            </button>
                            <button>Kill 1 Card</button>
                            <button>Challenge</button>
                          </div>
                        )}
                        {canChallenge1Player && (
                          <button onClick={() => challenge('first')}>
                            Challenge 1st Player
                          </button>
                        )}
                        {canChallenge2Player && (
                          <button onClick={() => challenge('second')}>
                            Challenge 2nd Player
                          </button>
                        )}
                        {canChallenge4Player && (
                          <button onClick={() => challenge('fourth')}>
                            Challenge 4th Player
                          </button>
                        )}
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('second')
                            }
                          }}
                        >
                          {player2Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('second')
                              }
                            }}
                          >
                            {player2Coins}
                          </div>
                          {turn === 'Player 2' && <Spinner />}
                        </div>
                      </div>
                      <div className='bot'>
                        <div>
                          {changingCards && (
                            <div>
                              {twoChangableCard.map((card, i) => (
                                <img
                                  alt='card'
                                  key={i}
                                  className={changedCardStyle}
                                  src={
                                    require(`../assets/cards-front/${card}.png`)
                                      .default
                                  }
                                  onClick={() => ChooseCard(card)}
                                />
                              ))}
                            </div>
                          )}
                          <div>
                            {magicianSkill && (
                              <button onClick={() => CardSkill('change')}>
                                Change Cards
                              </button>
                            )}
                            {assassinSkill && (
                              <button onClick={() => CardSkill('kill')}>
                                Kill with 3 Coin
                              </button>
                            )}
                            {dictatorSkill && (
                              <button onClick={() => CardSkill('pick')}>
                                Pick 3 Coin
                              </button>
                            )}
                            {thiefSkill && (
                              <button onClick={() => CardSkill('steal')}>
                                Steal 2 Coin
                              </button>
                            )}
                          </div>
                          <div ref={move}></div>
                          <img
                            alt='card'
                            className='Card'
                            src={require(`../assets/mystery-card.png`).default}
                            onClick={() => bluff()}
                          />
                          {player3Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className={KillUrCardsStyle}
                              src={
                                require(`../assets/cards-front/${item}.png`)
                                  .default
                              }
                              onClick={() => {
                                if (turn === 'Player 3') {
                                  if (KillUrCardsStyle === 'kill-style') {
                                    killYourCard(item)
                                  } else if (changingUrCard) {
                                    swap(item)
                                  } else {
                                    CardHandler(item)
                                  }
                                }
                              }}
                            />
                          ))}
                        </div>
                        <div className={changedCoinStyle}>{player3Coins}</div>
                        <br />
                        <a href='/'>
                          <button className='game-button'>QUIT</button>
                        </a>
                      </div>
                    </>
                  )}

                  {/* PLAYER 4 VIEW */}
                  {currentUser === 'Player 4' && (
                    <>
                      <div className='top'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('second')
                            }
                          }}
                        >
                          {player2Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                        </div>
                        <div
                          className={changedCoinStyle}
                          onClick={() => {
                            if (canSteal) {
                              steal('second')
                            }
                          }}
                        >
                          {player2Coins}
                        </div>
                        {turn === 'Player 2' && <Spinner />}
                      </div>
                      <div className='mid'>
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('first')
                            }
                          }}
                        >
                          {player1Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('first')
                              }
                            }}
                          >
                            {player1Coins}
                          </div>
                          {turn === 'Player 1' && <Spinner />}
                        </div>
                        <div>
                          {player4BeingChallenged && (
                            <h1>You Are Being Challenged</h1>
                          )}
                          {player4KillCard && (
                            <div>
                              <h3>You Lied, Kill 1 Card!</h3>
                            </div>
                          )}
                        </div>
                        {player4beingKilled && (
                          <div>
                            <h3> You are being killed</h3>
                            <button
                              style={{
                                backgroundColor: p4HaveCard ? 'green' : 'red',
                              }}
                              onClick={() => Block()}
                            >
                              Block
                            </button>
                            <button>Kill 1 Card</button>
                            <button>Challenge</button>
                          </div>
                        )}
                        {canChallenge1Player && (
                          <button onClick={() => challenge('first')}>
                            Challenge 1st Player
                          </button>
                        )}
                        {canChallenge2Player && (
                          <button onClick={() => challenge('second')}>
                            Challenge 2nd Player
                          </button>
                        )}
                        {canChallenge3Player && (
                          <button onClick={() => challenge('third')}>
                            Challenge 3rd Player
                          </button>
                        )}
                        <div
                          className={killStyleChanger}
                          onClick={() => {
                            if (killingPlayer) {
                              kill('third')
                            }
                          }}
                        >
                          {player3Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className='Card'
                              src={require(`../assets/cardback.png`).default}
                            />
                          ))}
                          <div
                            className={changedCoinStyle}
                            onClick={() => {
                              if (canSteal) {
                                steal('third')
                              }
                            }}
                          >
                            {player3Coins}
                          </div>
                          {turn === 'Player 3' && <Spinner />}
                        </div>
                      </div>
                      <div className='bot'>
                        <div>
                          {changingCards && (
                            <div>
                              {twoChangableCard.map((card, i) => (
                                <img
                                  alt='card'
                                  key={i}
                                  className={changedCardStyle}
                                  src={
                                    require(`../assets/cards-front/${card}.png`)
                                      .default
                                  }
                                  onClick={() => ChooseCard(card)}
                                />
                              ))}
                            </div>
                          )}
                          <div>
                            {magicianSkill && (
                              <button onClick={() => CardSkill('change')}>
                                Change Cards
                              </button>
                            )}
                            {assassinSkill && (
                              <button onClick={() => CardSkill('kill')}>
                                Kill with 3 Coin
                              </button>
                            )}
                            {dictatorSkill && (
                              <button onClick={() => CardSkill('pick')}>
                                Pick 3 Coin
                              </button>
                            )}
                            {thiefSkill && (
                              <button onClick={() => CardSkill('steal')}>
                                Steal 2 Coin
                              </button>
                            )}
                          </div>
                          <div ref={move}></div>
                          <img
                            alt='card'
                            className='Card'
                            src={require(`../assets/mystery-card.png`).default}
                            onClick={() => bluff()}
                          />
                          {player4Deck.map((item, i) => (
                            <img
                              alt='card'
                              key={i}
                              className={KillUrCardsStyle}
                              src={
                                require(`../assets/cards-front/${item}.png`)
                                  .default
                              }
                              onClick={() => {
                                if (turn === 'Player 4') {
                                  if (KillUrCardsStyle === 'kill-style') {
                                    killYourCard(item)
                                  } else if (changingUrCard) {
                                    swap(item)
                                  } else {
                                    CardHandler(item)
                                  }
                                }
                              }}
                            />
                          ))}
                        </div>
                        <div className={changedCoinStyle}>{player4Coins}</div>
                        <br />
                        <a href='/'>
                          <button className='game-button'>QUIT</button>
                        </a>
                      </div>
                    </>
                  )}
                </div>
              )}
            </>
          )}
        </>
      ) : (
        <>
          <h1>Room full</h1>
          <br />
          <a href='/'>
            <button className='game-button'>QUIT</button>
          </a>
        </>
      )}
    </div>
  )
}

export default Game
