import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import randomCodeGenerator from '../utils/randomCodeGenerator'

const Homepage = () => {
  const [roomCode, setRoomCode] = useState('')

  return (
    <div style={{ backgroundColor: 'black', height: '100vh' }}>
      <div className='homepage-menu'>
        <h1>BLUFF</h1>
        <div className='homepage-form'>
          <div className='homepage-join'>
            <input
              type='text'
              placeholder='Game Code'
              onChange={(event) => setRoomCode(event.target.value)}
            />
            <Link to={`/play?roomCode=${roomCode}`}>
              <button className='create-button red'>JOIN GAME</button>
            </Link>
          </div>
          <h1>OR</h1>
          <div className='homepage-create'>
            <Link to={`/play?roomCode=${randomCodeGenerator(5)}`}>
              <button className='create-button'>CREATE GAME</button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Homepage
