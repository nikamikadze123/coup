export default [
  {
    user: 'Player 1',
    left: 'Player 2',
    right: 'Player 3',
    up: 'Player 4',
  },
  {
    user: 'Player 2',
    left: 'Player 3',
    right: 'Player 4',
    up: 'Player 1',
  },
  {
    user: 'Player 3',
    left: 'Player 4',
    right: 'Player 1',
    up: 'Player 2',
  },
  {
    user: 'Player 4',
    left: 'Player 1',
    right: 'Player 2',
    up: 'Player 3',
  },
]
